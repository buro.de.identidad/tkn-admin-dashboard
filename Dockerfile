FROM openjdk:8-jre-alpine
COPY target/tkn-admin-dashboard-1.0.0.jar /home/tkn-admin-dashboard.jar
COPY script_init.sh /home/script_init.sh
RUN chmod 777 /home/script_init.sh
RUN mkdir -p /opt/bidserver/logs
EXPOSE 8762
ENTRYPOINT ["/home/script_init.sh"]
